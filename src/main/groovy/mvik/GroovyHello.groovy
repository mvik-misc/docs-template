package mvik

class GroovyHello {

    // tag::groovy-hello[]
    def hello(String name, List<String> args = []) {
        return "Hello ${name}, bring your own ${args.join(', ')}"
    }
    // end::groovy-hello[]
}
