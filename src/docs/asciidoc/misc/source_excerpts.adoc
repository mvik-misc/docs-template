= Random Java Code

== Complete File

.include an entire file
[source,groovy,indent=0,options="nowrap,linenums"]
----
include::{groovy-dir}/mvik/GroovyHello.groovy[]
----

== Parts of a File

.include parts of a file
[source,groovy,indent=0,options="nowrap"]
----
include::{groovy-dir}/mvik/GroovyHello.groovy[tags=groovy-hello]
----
